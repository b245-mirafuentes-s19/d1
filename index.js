console.log("hello world");

// What is conditional Statements?
	// Conditional statement allow us to control the flow of our program.
	// It allows us to run a statement/instruction if a condition is met or run another separate instruction if not otherwise

// [Section] If, else if and else statement

let numA = -1;

/*
	if Statement
		- it will execute the statement/code blocks if a specfied condition is met/true.
*/

if (numA<0) {
	console.log("hello from numA");
}
console.log(numA<0);

/*
	Syntax:
	if (condition){
		statement;
	}
*/
	// The result of the expression added in the if's condition must result to true, else the statement inside the if() will not run.

// Lets reassign the variable numA and run as if statement with the same condition

numA = 1

if (numA<0) {
	console.log("Hello from the reassigned value of numA");
}
console.log(numA<0);

// It will not run because the expression now results to false

let city = "New York";

if (city === "New York") {
	console.log("Welcome to New York!");
}

// else if clause
/*
	- Executes a statement if previous conditions are false and if the specified condition is true
	- The "else if" is optional and can be added to capture additional conditions to change the flow of a program.
*/

let numH = 1;

if (numH<0) {
	console.log("Hello from numH<0");
}
else if (numH>0) {
	console.log("hello from numH>0");
}

/*We were able to run the else if() statement after we evaluated that the if condition was failed/false*/

// If the if() condition was passed and run, we will no longer evaluate to else if() and end the process.

/*else if is dependent with if, you cannot use else if clause alone
{
	else if (numH>0) {
		console.log("hello from numH>0");
	}
}*/
// numH = 1;
if (numH !== 1) {
	console.log("hello from numH === 1");
}
else if (numH===1) {
	console.log("hello from numH>0");
}
else if (numH>0) {
	console.log("hello from second else if")
}

city = "Tokyo";

if (city === "New York") {
	console.log("Welcome to New York!");
}
else if (city === "Tokyo") {
	console.log("Welcom to Tokyo");
}

// else statement
/*
	- Execute a statement if all other conditions are false/not met
	- else if statement is optional and can be added to capture any other result to change the flow of program.

*/

numH = 2;
// false
if (numH<0) {
	console.log("hello from if statement");
}
// false
else if(numH>2){
	console.log("hello from the first else if")
}
// false
else if (numH>3) {
	console.log("hello from the second else if")
}
// since all of the condition above are false/not met, else statement will run
else{
	console.log("hello from the else statement")
}

// Since all of the preceeding if and else conditions were not met, the else statement was executed instead.
// else statement is also depended with if statement, it cannot go alone

/*{
	else{
		console.log("hello from the else inside the code block");
	}
}*/

	// if, else if and else statement with functions
	/*
		Most of the times we would like to use if, else if and if statement with functions to control the flow of our application
	*/

let message;

function determineTyphoonIntensity(windSpeed){
	if (windSpeed<0) {
		return "Invalid argument!";
	}
	else if (windSpeed<=30){
		return "not a typhoone yet!";
	}
	else if (windSpeed<=60){
		return "tropical depression";
	}
	else if (windSpeed<=89) {
		return "tropical storm detected!"
	}
	else if (windSpeed<=118) {
		return "severe tropical storm detected"
	}
	else if (windSpeed > 118) {
		return "typhoon detected"
	}
	else{
		return "invalid argument"
	}
}

	// mini activity
	// 1. add return tropical storm detected if the windspeed is between 60 and 89
	// 2. add return severe tropical storm detected if the windspeed is between 88 and 118
	// 3. if higher than 117 return typhoon detected
console.log(determineTyphoonIntensity("q"));

	message = determineTyphoonIntensity(119);

	if (message === "typhoon detected"){
		// console.warn() is a good way to print warning in our console that could help us developers act on certain output within our code.
		console.warn(message);
	}

// [Section] Truthy or Falsy
	// Javascript a "truthy" is a value that is considered a true when encountered in boolean context
	// values are considered true unless defined otherwise

	// falsy values/ exceptions for truthy
	/*
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN - not a number
	*/

if (true) {
	console.log("true");
}

if (1) {
	console.log("true");	
}

if (null) {
	console.log("hello from null inside the if condition");
}
else{
	console.log("hello from the else null condition");
}

// [Section] Condition operator/ Ternary operator
	/*
		The Conditional Operator
		1. condition/expression
		2. expression to execute if the condition is true or truthy
		3. expression if the condition is falsy
		Syntax:
		(expression) ? true : false;
	*/

	let ternaryResult = (1>18) ? 1 : 2;
	console.log(ternaryResult);

// [Section] Switch Statement
	/*
		The switch statement evaluates an expression and matches the expression's value to a case class.
	*/
	// toLowerCase() method will convert all the letters into small letters
	let day = prompt("What day of the week today").toLowerCase();
	switch (day){
		case "monday":
			console.log("The color of the day is red!");
			// it means the code will stop here
			break;
		case 'tuesday':
			console.log('the color of the day is orange');
			break;
		case "wednesday":
			console.log("the color of the day is yellow");
			break;
		case "thursday":
			console.log("the color of the day is green");
			break;
		case "friday":
			console.log("the color of the day is blue");
			break;
		case "saturday":
			console.log("the color of the day is indigo");
			break;
		case "sunday":
			console.log("the color of the day is violet");
			break;

		default:
			console.log("Please input valid day")
			break;
	}
		
// [Section] Try-catch-finally
	// try-catch statement for error handling
	// There are instances when the application returns an error/warning that is not necessarily an error in the context of our code.
	// These errors are result of an attempt of the programming language to help developers in creating efficient code.

	function showIntensity(windSpeed){
		try{
			// codes that will be executed or run
			alerat(determineTyphoonIntensity(windSpeed));
			alert("Intensity updates will show alert from try")
		}
		catch(error){
			console.warn(error.message)
		}
		finally{
			// Continue execution of code regardless of success and failure of code execution in the try statement
			alert("Intensity updates will show new alert from finally")
		}
	}

	showIntensity(119);